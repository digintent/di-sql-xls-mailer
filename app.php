<?php
require_once __DIR__ . '/vendor/autoload.php';

$options = getopt('c:');

if (!isset($options['c']))
{
	die('You must pass the configuration file with the -c switch.' . PHP_EOL);
}

$config = @include $options['c'];

if (!is_array($config))
{
	die('Invalid or missing config file: ' . $options['c'] . PHP_EOL);
}

try
{
	$db = new PDO($config['database']['dsn'], $config['database']['username'], $config['database']['password'], $config['database']['options']);
}
catch (PDOException $e)
{
	die('Unable to connect to database. ' . $e->getMessage() . PHP_EOL);
}

$xls = new PHPExcel();

$sheet_number = 0;

foreach ($config['sheets'] as $sheet_config)
{
	if ($sheet_number > 0)
	{
		$xls->createSheet();
	}

	$xls->setActiveSheetIndex($sheet_number++);
	$sheet = $xls->getActiveSheet();
	$sheet->setTitle($sheet_config['name']);

	$y = 1;
	$x = 'A';

	$query = $db->query($sheet_config['query']);

	for ($i = 0; $i < $query->columnCount(); ++$i, ++$x)
	{
		$meta = $query->getColumnMeta($i);
		$sheet->setCellValue($x . $y, $meta['name']);

		$sheet->getColumnDimension($x)->setAutoSize(true);
	}

	while (($row = $query->fetch(PDO::FETCH_ASSOC)) !== false)
	{
		++$y;
		$x = 'A';

		foreach ($row as $value)
		{
			$sheet->setCellValue($x . $y, $value);
			++$x;
		}
	}
}

$xls->setActiveSheetIndex(0);

$filename = tempnam(sys_get_temp_dir(), 'xls');

$objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
$objWriter->save($filename);

$mail = new PHPMailer;

$mail->isSMTP();
$mail->Host = $config['mailer']['host'];
$mail->SMTPAuth = $config['mailer']['auth'];

if ($config['mailer']['auth'] === true)
{
	$mail->Username = $config['mailer']['username'];
	$mail->Password = $config['mailer']['password'];
	$mail->SMTPSecure = $config['mailer']['encryption'];
}

$mail->From = $config['email']['from'];
$mail->FromName = $config['email']['from_name'];

foreach ($config['email']['to'] as $email => $name)
{
	$mail->addAddress($email, $name);
}

$mail->addAttachment($filename, $config['filename']);
$mail->isHTML(true);

$mail->Subject = $config['email']['subject'];
$mail->Body = $config['email']['body'];

if (!$mail->send())
{
	die('Message could not be sent. ' . $mail->ErrorInfo . PHP_EOL);
}

unlink($filename);