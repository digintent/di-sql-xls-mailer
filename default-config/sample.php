<?php
return [
	'database' => [
		'dsn' => 'mysql:host=127.0.0.1;dbname=;charset=utf8mb4',
		'username' => '',
		'password' => '',
		'options' => []
	],

	'mailer' => [
		'host' => 'localhost',
		'auth' => false,
		'username' => '',
		'password' => '',
		'encryption' => 'tls',
	],

	'email' => [
		'from' => 'sender@domain.com',
		'from_name' => 'Sender Name',
		'to' => [
			'recipient@domain.com' => 'Recipient Name'
		],
		'subject' => '',
		'body' => '',
	],

	'filename' => 'report-' . date('Y-m-d') . '.xls',

	'sheets' => [[
		'name' => 'Foo',
		'query' => 'SELECT * FROM foo'
	]],
];